﻿using System;
using System.Threading.Tasks;

namespace helloWorld
{
    class Program
    {
        //For test : dotnet run -- Should I use top level statements in all my programs?
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        
            foreach (string arg in args)
            {
                Console.Write(arg + " ");
            }

            string[] answers =
            {
                "It is certain.",       "Reply hazy, try again.",     "Don’t count on it.",
                "It is decidedly so.",  "Ask again later.",           "My reply is no.",
                "Without a doubt.",     "Better not tell you now.",   "My sources say no.",
                "Yes – definitely.",    "Cannot predict now.",        "Outlook not so good.",
                "You may rely on it.",  "Concentrate and ask again.", "Very doubtful.",
                "As I see it, yes.",
                "Most likely.",
                "Outlook good.",
                "Yes.",
                "Signs point to yes.",
            };

            await AnimationAsync();

            var index = new Random().Next(answers.Length - 1);
            Console.WriteLine(answers[index]);
        }

        private static async Task AnimationAsync()
        {
            foreach (string s in new[] { "| -", "/ \\", "- |", "\\ /", })
            {
                Console.Write(s);
                await Task.Delay(100);
                Console.Write("\b\b\b");
            }
        }
    }
}
